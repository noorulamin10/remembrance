//
//  TodoDC.swift
//  Todo
//
//  Created by Inam on 24/07/2015.
//  Copyright (c) 2015 Noor. All rights reserved.
//

import UIKit

class TodoDC: NSObject {
    // MARK: Properties
    
    var name: String
    var photo: UIImage?
    var rating: Int
    
    // MARK: Initialization
    
    init?(name: String, photo: UIImage?, rating: Int) {
        // Initialize stored properties.
        self.name = name
        self.photo = photo
        self.rating = rating
    }
    
}
