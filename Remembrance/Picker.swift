//
//  Picker.swift
//  Todo
//
//  Created by Inam on 30/07/2015.
//  Copyright (c) 2015 Noor. All rights reserved.
//

import Foundation
import CoreData

class Picker: NSManagedObject {

    @NSManaged var category: String
    @NSManaged var image_name: String

}
