//
//  Events.swift
//  Todo
//
//  Created by Inam on 04/08/2015.
//  Copyright (c) 2015 Noor. All rights reserved.
//

import Foundation
import CoreData

class Events: NSManagedObject {

    @NSManaged var day: String
    @NSManaged var id: NSNumber
    @NSManaged var month: String
    @NSManaged var year: String
    @NSManaged var title: String

}
