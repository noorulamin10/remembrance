//
//  CustomSettings.swift
//  Todo
//
//  Created by Inam on 29/07/2015.
//  Copyright (c) 2015 Noor. All rights reserved.
//

import UIKit
import CoreData
class CustomSettings: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    var category = ""
    var imgString = ""
    var strtDate : String = String()
    let managedObjectContext =
    (UIApplication.sharedApplication().delegate
        as! AppDelegate).managedObjectContext
 var categoryArray : [String] = ["Birthday ", "Holiday", "School", "Life", "Religion" ]
    @IBAction func btnSave(sender: AnyObject) {
        self.saveEventsforCalandar()
        self.willMoveToParentViewController(nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
    }
    @IBAction func btnCancel(sender: AnyObject) {
        self.willMoveToParentViewController(nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
        
    
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"Picker")
        
        //3
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as? [NSManagedObject]
        
        if let results = fetchedResults {
            //additonal
            let match = results[0] as NSManagedObject
            
            category = match.valueForKey("category") as! String
            imgString = match.valueForKey("image_name") as! String
            ////////////
          println(" \(category)")
          println("hence result : \(results.count)")
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        
        
    }
    @IBOutlet var txtEvent: UITextField!
    @IBOutlet var categoryPicker: UIPickerView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBAction func btnAddCategory(sender: AnyObject) {
        var alert = UIAlertController(title: "Add New Category", message: "Would you like to add a new category?", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
            
            
            
            println("User clicking agauna aahaas Ok button")
            println(self.txtEvent.text)
            self.categoryArray.append(self.txtEvent.text)
            self.categoryPicker .reloadAllComponents()
            self.savePickerItemsIntoCoreData()
        }))
        self.presentViewController(alert, animated: true, completion: {
            println("completion block")
            
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func configurationTextField(textField: UITextField!)
    {
        println("configurat hire the TextField")
        
        if let tField = textField {
            
            self.txtEvent = textField!        //Save reference to the UITextField
           
            
        }
    }
    
    
    func handleCancel(alertView: UIAlertAction!)
    {
        println("User click Cancel button")
        println(self.txtEvent.text)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categoryArray.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return categoryArray[row]
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       txtEvent.text = "\(categoryArray[row])" // neeed for change ...
    
    }
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 35
    }
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var customView = UIView (frame: CGRectMake(0, 0, pickerView.bounds.width-30, 30))
        var imgView = UIImageView(frame: CGRectMake(0, 0, 30, 30))
        
        switch row {
            
        case 0:
            
            imgView.image = UIImage(named:"calandar-icon.png")
        case 1:
            
            imgView.image = UIImage(named:"home-icon.png")
        case 2:
            
            imgView.image = UIImage(named:"icloud-icon.png")
        
        default:
            
            imgView.image = nil
        }
        customView.backgroundColor = UIColor .blackColor()
        customView.addSubview(imgView)
        
        return customView
    }

//----- create items for picker view and saving it into coredata
func savePickerItemsIntoCoreData()
{
    let entityDesicrition = NSEntityDescription.entityForName("Picker", inManagedObjectContext: managedObjectContext!)
    let addCategories =  Picker(entity: entityDesicrition!,
        insertIntoManagedObjectContext: managedObjectContext)
    addCategories.category = self.txtEvent.text
    addCategories.image_name = "icloud-icon"
    var error:NSError?
    managedObjectContext?.save(&error)
    if let err = error{
        println(err.localizedFailureReason)
    }
    else
    {
        println("saved")
    }
 
}
    @IBAction func datePickerAction(sender: AnyObject) {
        var dateformattar = NSDateFormatter()
        dateformattar.dateFormat = "dd-MM-yyyy"
         strtDate = dateformattar.stringFromDate(datePicker.date)
        println(strtDate)
    }
    func saveEventsforCalandar()
    {
        let entityDesicrition = NSEntityDescription.entityForName("Events", inManagedObjectContext: managedObjectContext!)
        let event =  Events(entity: entityDesicrition!,
            insertIntoManagedObjectContext: managedObjectContext) as Events
        // convert selected date into components
        let flags: NSCalendarUnit = .DayCalendarUnit | .MonthCalendarUnit | .YearCalendarUnit
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let convertedDate = dateFormatter.dateFromString(strtDate)
        let date = convertedDate
        let components = NSCalendar.currentCalendar().components(flags, fromDate: date!)
        //converting components into day month and year and saving into entity
        event.day = String(components.day)
        event.month = String(components.month)
        event.year = String(components.year)
        
        var error:NSError?
        managedObjectContext?.save(&error)
        if let err = error{
            println(err.localizedFailureReason)
        }
        else
        {
            println("saved")
        }
    }
}
