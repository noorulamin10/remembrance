//
//  CalanderVC.swift
//  Todo
//
//  Created by Inam on 27/07/2015.
//  Copyright (c) 2015 Noor. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
class CalanderVC: UIViewController {
    
    @IBOutlet var reminderText: UITextField!
    @IBOutlet var myDatePicker: UIDatePicker!
       override func viewDidLoad() {
        super.viewDidLoad()
        
        var eventStore = EKEventStore()
        eventStore.requestAccessToEntityType(EKEntityTypeReminder,
            completion: {(granted: Bool, error:NSError!) in
                if !granted {
                    println("Access to store not granted")
                }
        })
        //Accessing Calendars in the Database
        let calendars =
        eventStore.calendarsForEntityType(EKEntityTypeReminder)
        
        for calendar in calendars as! [EKCalendar] {
            println("Calendar = \(calendar.title)")
        }
        //Creating Reminders
        let reminder = EKReminder(eventStore: eventStore)
        reminder.title = "Go to the store and buy milk"
        reminder.calendar = eventStore.defaultCalendarForNewReminders()
        var error: NSError?
        eventStore.saveReminder(reminder, commit: true, error: &error)
        
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func setReminder(sender: AnyObject) {
      let  appDelegate = UIApplication.sharedApplication().delegate
            as? AppDelegate
        
        if appDelegate!.eventStore == nil {
            appDelegate!.eventStore = EKEventStore()
            appDelegate!.eventStore!.requestAccessToEntityType(
                EKEntityTypeReminder, completion: {(granted, error) in
                    if !granted {
                        println("Access to store not granted")
                        println(error.localizedDescription)
                    } else {
                        println("Access granted")
                    }
            })
        }
        
        if (appDelegate!.eventStore != nil) {
            self.createReminder()
        }
        reminderText.resignFirstResponder()
        
    }
    func createReminder() {
        let  appDelegate = UIApplication.sharedApplication().delegate
            as? AppDelegate
        let reminder = EKReminder(eventStore: appDelegate!.eventStore)
        
        reminder.title = reminderText.text
        reminder.calendar =
            appDelegate!.eventStore!.defaultCalendarForNewReminders()
        let date = myDatePicker.date
        let alarm = EKAlarm(absoluteDate: date)
        
        reminder.addAlarm(alarm)
        
        var error: NSError?
        appDelegate!.eventStore!.saveReminder(reminder,
            commit: true, error: &error)
        
        if error != nil {
            println("Reminder failed with error \(error?.localizedDescription)")
        }
    }
    
///-----
   
}
